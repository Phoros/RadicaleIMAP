# Radicale IMAP

IMAP authentication plugin for [Radicale](http://radicale.org/).

## Installation

```shell
$ python3 -m pip install --upgrade git+https://gitlab.com/Phoros/RadicaleIMAP
```

## Configuration

```ini
[auth]
type = radicale_imap

# IMAP server host name
# Syntax: address:port
# For example: imap.server.tld
#imap_host =

# Use SSL or StartTLS to secure the connection
# Requires Python >= 3.4
# Syntax: SSL|STARTTLS
#imap_ssl_type = 
```

## License

[GPL-3.0](https://github.com/Unrud/RadicaleIMAP/blob/master/COPYING)

## Contribution
Original author: [Unrud](https://github.com/Unrud)
Modifications by: [Phoros](https://gitlab.com/Phoros)